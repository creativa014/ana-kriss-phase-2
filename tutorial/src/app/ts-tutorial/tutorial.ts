const myNumber: number = 23;

const myName: string = 'vukashin';

const myDate: Date = new Date(2021, 1, 22);

const amIAlwaysRight: boolean = false;


interface IBaseEntity {
    id: string;
    createdAt?: string;
    updatedAt?: string;
    createdBy?: string;
    deletedAt: Date;
    _rowVersion?: string;

}

abstract class BaseEntity implements IBaseEntity {
    constructor(id: string) {
        this.id = id;
    }

    id: string;
    createdAt?: string;
    updatedAt?: string;
    createdBy?: string;
    _rowVersion?: string;
    deletedAt: Date;
}

class Company extends BaseEntity {
    constructor(id: string, name: string, address: string) {
        super(id);
        this.name = name;
        this.address = address;
    }

    name: string;
    address: string;
    contactPerson: Person;
    //.....
}

enum Gender {
    female = 'female',
    male = 'male'
}

class Person extends BaseEntity {
    constructor(id: string, firstName: string) {
        super(id);
        this.firstName = firstName;
    }

    firstName: string;
    lastName?: string;
    gender?: Gender;
}

const person = new Person('123', 'name');
person.gender = Gender.male; // { ... , gender: 'female' }
person.gender = Gender.female; // { ... , gender: 'male' }
